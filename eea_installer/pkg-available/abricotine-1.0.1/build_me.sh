#!/bin/bash
source ../../utilities.sh

banner "BUILDING ABRICOTINE MARKDOWN READER AND EDITOR PACKAGE"

WORKDIR=$(preparePackage)
echo $WORKDIR
rm abricotine*.tar.gz
rm -rf abricotine*.tar
wget https://github.com/brrd/abricotine/releases/download/1.0.0/abricotine-1.0.0-linux-x64.tar.gz
gunzip abricotine-1.0.0-linux-x64.tar.gz
tar -vxf abricotine-1.0.0-linux-x64.tar -C $WORKDIR/opt
mv $WORKDIR/opt/Abricotine-linux-x64 $WORKDIR/opt/abricotine
chown -R root $WORKDIR/opt/abricotine
chgrp -R root $WORKDIR/opt/abricotine
chmod -R 644 $WORKDIR/opt/abricotine
find $WORKDIR/opt/abricotine/ -type d -exec chmod +x {} \;
chmod +x $WORKDIR/opt/abricotine/abricotine

finalizePackage $WORKDIR
