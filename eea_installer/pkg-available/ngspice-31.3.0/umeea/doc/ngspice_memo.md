### Memo NgSPICE
Une fois ngspice lancé :
- la commande `cd` permet de connaitre le chemin courant et de se déplacer dans le système de fichiers, comme dans un terminal habituel
- la commande `edit` suivie d'un nom de fichier netlist permet d'éditer la netlist
- la commande `source` suivie d'un nom de fichier netlist d'activer la netlist
- comme dans SPICE, les commandes `op`, `dc`, `ac` et `tran` sont disponibles 
- comme dans SPICE, `*` en début de ligne fait de la ligne un commentaire, `;` permet de faire un commentaire en cours de ligne. Les lignes commentées ne sont pas exécutées

### Exemple 
Pour simuler le transitoire temporel et le diagramme de Bode du circuit ci-dessous :
![alt text](./figRCRC.png)

1. tapez `edit RCRC.cir`. Une fois l'éditeur lancé, copiez le code ci-dessous à l'intérieur, puis sauvez :
```
.title double RC en cascade
R1 int in 10k
V1 in 0 dc 0 PULSE (0 5 1u 1u 1u 1 1)
R2 out int 1k
C1 int 0 1u
C2 out 0 100n
.end
```

2. retournez dans la fenetre de ngspice et tapez :
`source RCRC.cir`

3. ensuite vous pouvez utiliser les commandes spice habituelles. Par exemple 'op' donne le point de fonctionnement. Sur cet exemple on peut vouloir par exemple calculer un régime transitoire. On peut l'obtenir en tapant :
`tran 50u 50m`

Ensuite, on peut regarder le résultat :
`plot in out`

4. On peut tracer aussi le diagramme de Bode. Il faut changer le type de source pour qu'elle soit sinusoïdale pure (condition normale pour réaliser un diagramme de Bode). Tapez `edit RCRC.cir` et modifiez le comme suit :
```
.title double RC en cascade
R1 int in 10k
*V1 in 0 dc 0 PULSE (0 5 1u 1u 1u 1 1)
V1 in 0 dc 0V ac 1V
R2 out int 1k
C1 int 0 1u
C2 out 0 100n
.end
```
L'autre source a été mise en commentaire ici avec le `*`

Ensuite dans ngspice, taper : 
```
ac dec 10 0.01 1k
plot in int out
```
ou on peut remplacer le plot pour un résultat en dB :
```
plot 20*log10(out) 20*log10(int) 20*log10(in)
```
