#!/bin/bash
source ../../utilities.sh

banner "BUILDING CUSTOM ENVIRONMENT FEATURES PACKAGE"

WORKDIR=$(preparePackage)
echo $WORKDIR

# all done by finalizePackage : etc as well as desktop customization

finalizePackage $WORKDIR
