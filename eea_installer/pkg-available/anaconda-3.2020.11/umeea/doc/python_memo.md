### Memo Python

#### Tracer des courbes
Pour tracer une courbe :
- vous devez d'abord importer `numpy` et `matplotlib`
- puis un ensemble de valeurs, sous la forme d'un vecteur, pour lesquelles il faut évaluer la fonction avec `np.linspace`
- puis évaluer la fonction elle même en vous basant sur la variable définie ci-dessus avec `np.linspace`
- Puis tracer la courbe avec `plt.plot`
*Sur un exemple :*
```
import numpy as np
import matplotlib.pyplot as plt

t=np.linspace(0,10,1000)
y=np.exp(-t)*np.sin(5*t)

plt.plot(t,y)
```
*Résultat :*
![alt text](/home/eea/usb-drive/eea_installer/pkg-available/anaconda/eea/doc/python_memo_curve.png)
#### Syntaxe listes, dictionnaires, vecteurs, matrices
##### Exemple de liste
```
divers=[1,'abc',3,4,5,"texte"]
print(divers[0])
print(divers[1])
print(divers[-2])
divers = divers + ["aa",5,2]
print(divers)
```
##### Exemple de dictionnaire
```
taille_daltons={'Joe':1.10,'William':1.35,'Jack':1.60,'Averell':1.85}
print(taille_daltons)
print(taille_daltons.keys())
print(taille_daltons['Jack'])
taille_daltons['Ma']=1.4
print(taille_daltons)
```
##### Exemple de vecteurs
import numpy as np
v1=np.zeros(5)
print(v1)
x=np.linspace(0,15,100)
print(x)
y=np.exp(-2*x) * np.sin(10*x)
print(y)
##### Exemple de matrices


#### Syntaxe if, for, etc
Quelques exemples "en vrac"
```
if a==True:
	b=5
```



	
