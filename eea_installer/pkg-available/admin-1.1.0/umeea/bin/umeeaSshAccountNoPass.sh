#!/bin/bash
# we create a ssh key to have never more to use ssh pass. Only two connections will be performed with sshpass.


HOST=""
SSHPASS=""
USER_NAME=$USER
CLEAR_EXISTING_KEYS="0"

source ~/.config/umeea/profile.conf 2> /dev/null

if [[ -v SSH_ACCOUNT ]]  ; then
  HOST=$SSH_ACCOUNT
fi



while [[ "$#" -ge 1 ]]; do

    case "$1" in
        -H)
            HOST="$2"
            shift; shift
            ;;
        -P)
            SSHPASS="$2"
            shift; shift
            ;;
        -U)
            USER_NAME="$2"
            shift; shift
            ;;
        -C)
            CLEAR_EXISTING_KEYS="1"
            shift; 
            ;;
        -h)
            echo
            echo '# Supported commands are :'
            echo ' -H name : [Mandatory] defines the distant Host, including account informations. "name" is the name of the host.'
            echo ' -P pass : [Mandatory] defines the Host password. "pass" is the password.'
            echo ' -U name : [Optionnal] user local account name. "name" is the name itself.'
            echo ' -C : [Optionnal] clears actual ssh keys configuration (private/public keys). In other cases, the script will try to use existing keys.'
            echo
            exit 0
            ;;
        *)
            echo "Unknown or badly placed parameter '$1'. Please run command -h for help" 1>&2
            exit 1
            ;;
    esac
done

if [[ "$SSHPASS" = "" || "$HOST" = ""  || "$USER_NAME" = "" ]]; then
    echo "Incorrect call to this script ! please run with -h for help !"
    exit 1
fi

USER_HOME="$(getent passwd $USER_NAME| cut -d: -f6)"
HOST_DOMAIN="$(python3 -c 'import re;import sys;regex = re.compile(r"^.+@(.+)$");print(regex.findall(sys.argv[1])[0],end="")' $HOST)"


#echo
#echo
#echo $SSHPASS
#echo $HOST
#echo $USER_NAME
#echo $USER_HOME
#echo $HOST_DOMAIN
#echo
#echo


if [[ "$CLEAR_EXISTING_KEYS" -eq '1' ]]; then
    rm "$USER_HOME/.ssh/id_rsa" 2> /dev/null
    rm "$USER_HOME/.ssh/id_rsa.pub" 2> /dev/null
fi



if [[ ! -f "$USER_HOME/.ssh/id_rsa" || ! -f "$USER_HOME/.ssh/id_rsa.pub" ]]; then

    if [[ -f "$USER_HOME/.ssh/id_rsa" || -f "$USER_HOME/.ssh/id_rsa.pub" ]]; then     
        echo "Your keys configuration is unclear for this peace of software. If you have no dependency to other ssh'd hosts, you should think about running this script again and add the -c options, that will clear your actual ssh keys configuration. At your own risk.'"
        exit 1
    fi
    # now we can create the keys safely
    mkdir  -p $USER_HOME/.ssh
    echo | ssh-keygen -t rsa -f $USER_HOME/.ssh/id_rsa
    chown -R $USER_NAME "$USER_HOME/.ssh/."
    chgrp -R $USER_NAME "$USER_HOME/.ssh/."
fi

# now copy public key to host
sshpass -p $SSHPASS  ssh $HOST  mkdir -p .ssh
cat "$USER_HOME/.ssh/id_rsa.pub"
cat "$USER_HOME/.ssh/id_rsa.pub" | sshpass -p $SSHPASS ssh -o StrictHostkeyChecking=no $HOST  'cat >> .ssh/authorized_keys'
ssh-keyscan -H $HOST_DOMAIN >> "$USER_HOME/.ssh/known_hosts"
chown -R $USER_NAME "$USER_HOME/.ssh/known_hosts"
#su -c "ssh $HOST exit" - $USER_NAME
ssh $HOST -o 'BatchMode=yes' -o 'ConnectTimeout=5' exit
exit 0
