#!/bin/bash
UMEEA_PATH="/opt/umeea"
source /etc/umeea/default.conf 2> /dev/null

echo "# Adding possible new repositories ..."
apt-get -y install umeea-extra-repositories
echo "# Downloading update descriptions ..."
apt-get -y update
echo "# Now updating EEA software ..."
apt-get -y install umeea-full-install
echo "# Done."

