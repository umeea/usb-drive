#!/bin/bash
UMEEA_PATH="/opt/umeea"
source /etc/umeea/default.conf 2> /dev/null



UPDATE_AVAILABLE=$($UMEEA_PATH/bin/umeeaUpdateChecker.sh)

if [ ! "$UPDATE_AVAILABLE" -eq "0" ]; then
notify-send -t 120000 -i "/opt/umeea/media/logo_eea_256.png" "Departement EEA Mises à jour" "Lancez les mises a jour dans le menu des logiciels > UM&EEA > Nouveaux Logiciels EEA"
fi
