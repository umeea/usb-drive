#!/bin/bash
UMEEA_PATH="/opt/umeea"
source /etc/umeea/default.conf

source ~/.config/umeea/profile.conf 2> /dev/null
OPEN_XDG_WINDOW="0"


while [[ "$#" -ge 1 ]]; do

    case "$1" in
        -H)
            SSH_ACCOUNT="$2"
            shift; shift
            ;;

        -x)
            OPEN_XDG_WINDOW="1"
            shift; 
            ;;
        -h)
            echo
            echo '# Supported commands are :'
            echo ' -H name : defines the distant Host, including account informations. "name" is the name of the host. Overrides the profile.conf data.'
            echo ' -x : open window of the mounted drive'
            echo
            exit 0
            ;;
        *)
            echo "Unknown or badly placed parameter '$1'. Please run command -h for help" 1>&2
            exit 1
            ;;
    esac
done







if [[ ! -v SSH_ACCOUNT ]]  ; then
        echo "This script requires the distant ssh account as parameter !"
        echo "pass it as script parameter or feed the SSH_ACCOUNT variable in .config/umeea/profile.conf. Run -h options for help."
        exit 1
fi


if [[ ! -v LOCAL_SSHFS_FOLDER ]]  ; then
    LOCAL_SSHFS_FOLDER='SshfsFolder'
fi




$UMEEA_PATH/bin/umeeaUmountUniversityAccount.sh
LOCAL_FOLDER="$HOME/$LOCAL_SSHFS_FOLDER"
mkdir -p "$LOCAL_FOLDER"
chown $USER "$LOCAL_FOLDER"
sshfs  $SSH_ACCOUNT: "$LOCAL_FOLDER"
mkdir -p $HOME/Bureau
ln -s  "$LOCAL_FOLDER" "$HOME/Bureau/$LOCAL_SSHFS_FOLDER"
if [[ $OPEN_XDG_WINDOW == "1" ]]; then
    xdg-open "$HOME/Bureau/$LOCAL_SSHFS_FOLDER"
fi
exit 0
