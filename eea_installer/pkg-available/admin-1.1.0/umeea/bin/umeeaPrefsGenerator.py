#import sys

#options={'--': 4098, 'sape': 4139}

#for k,arg in enumerate(sys.argv):
#    print(arg)

#print(options)

import re
from pathlib import Path
import os


testKeywords={'um_user_email':'mikhael.myara@umontpellier.fr'}

# usual keywords I use : um_user_email, um_user_first_name, um_user_last_name, um_user_password
def createPrefByTemplate(templateName,outName,knownKeywords):


    theTemplateFile = open(templateName, 'r')
    theOutputFile = open(outName, 'w')


    for line in theTemplateFile :       
        newline = line
        for kw,value in knownKeywords.items():
            newline=re.sub("(<@@"+kw+"@@>)",value,newline)
        theOutputFile.write(newline)


def create_x2go():
    home = str(Path.home())
    workpath = os.path.join(home,'.x2goclient')
    os.mkdir(workpath )
    workFile = os.path.join(workpath,'sessions')
    createPrefByTemplate('templates/x2go.session.template',workFile,testKeywords)

create_x2go()

