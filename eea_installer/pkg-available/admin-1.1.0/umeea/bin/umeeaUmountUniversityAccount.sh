#!/bin/bash

source ~/.config/umeea/profile.conf 2> /dev/null

if [[ ! -v LOCAL_SSHFS_FOLDER ]]  ; then
    LOCAL_SSHFS_FOLDER='SshfsFolder'
fi

umount "$HOME/Bureau/$LOCAL_SSHFS_FOLDER" 2> /dev/null
rm "$HOME/Bureau/$LOCAL_SSHFS_FOLDER" 2> /dev/null
rm "$HOME/$LOCAL_SSHFS_FOLDER" 2> /dev/null
