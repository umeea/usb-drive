#!/bin/bash
HOST=""
SSHPASS=""
USER_NAME=$USER
CLEAR_EXISTING_KEYS="0"

source ~/.config/umeea/profile.conf 2> /dev/null

if [[ -v SSH_ACCOUNT ]]  ; then
  HOST=$SSH_ACCOUNT
fi

while [[ "$#" -ge 1 ]]; do

    case "$1" in
        -H)
            HOST="$2"
            shift; shift
            ;;
        -P)
            SSHPASS="$2"
            shift; shift
            ;;

        -h)
            echo
            echo '# Supported commands are :'
            echo ' -H name : defines the distant Host, including account informations. "name" is the name of the host.'
            echo ' -P pass : defines the Host password. "pass" is the password.'
            echo
            exit 0
            ;;
        *)
            echo "Unknown or badly placed parameter '$1'. Please run command -h for help" 1>&2
            exit 1
            ;;
    esac
done
#ssh -q -o 'BatchMode=yes' -o 'ConnectTimeout=5' $SSH_ACCOUNT 'echo 2>&1' && echo $host SSH_OK || echo $host SSH_NOK
COMMAND="ssh -q -o 'BatchMode=yes' -o 'ConnectTimeout=5' $SSH_ACCOUNT 'echo 2>&1' && echo $host SSH_OK || echo $host SSH_NOK"
if [[  -z SSHPASS ]]; then
  COMMAND="sshpass -p $SSHPASS ${COMMAND}" 
fi

#COMMAND

eval $COMMAND
