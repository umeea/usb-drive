#!/opt/anaconda3/bin/python3
from PyQt5.QtCore import QObject, QThread, pyqtSignal,QTimer,QMutex,Qt
from PyQt5 import QtWidgets,QtCore,QtGui
import pyqtgraph as pg
import numpy as np
from datetime import datetime
import time,base64,os,sys
 

def encodeFile(fileName):
    SVGFileName = fileName
    f=open(SVGFileName,'rb')
    svgdata = f.read()
    encoded = base64.b64encode(svgdata).decode("utf-8")
    f.close()
    return encoded

def logo_Qt(fileName,width=0,height=0):
    fullPathName = '/opt/umeea/media/'+fileName
    png =   encodeFile(fullPathName)
    widthstr=''
    heightstr=''
    if width != 0:
        widthstr='width="{0} "'.format(width)
    if height != 0:
        heightstr='height="{0} "'.format(width)
    return '<img alt="'+fileName+'" {0}{1}src="data:image/png;base64,{2}" />'.format(str(heightstr),str(widthstr),png)


def eeaWidgetHTML_Qt(credits=None):

    logoEEA = logo_Qt('logoEEA-50px.png')
    logoUM = logo_Qt('logoUM-50px.png')
    logoFDS = logo_Qt('logoFDS-50px.png')
    creditsLine =  ""
    if credits is not None:
        creditsLine = "<td width='99%' style='text-align:left;' ><p style='margin-left:15px;font-size:small'>{0}</p></td>".format(credits)

    table = "<hr width='100%'>"+"""<table border='0' width='100%' style='margin: 0px;padding:0px;'>
        <tr>
            <td style='text-align:left !important;white-space: nowrap; padding-right:15px;'>{0}</td>
            <td style='white-space: nowrap;padding-right:15px;'>{1}</td>
            <td style='text-align:right !important:white-space: nowrap;padding-right:15px;'>{2}</td>
            {3}
       </tr>
    </table>""".format(logoEEA,logoUM,logoFDS,creditsLine)
    #return logoEEA
    return table



class infoWindow(QtWidgets.QWidget):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.setWindowTitle('Mise à jour et Installation de Nouveaux Logiciels par le Département EEA')
        self.setMinimumWidth(700)
        self.setMinimumHeight(500)

        self.Gui={}


        self.Gui['terminal'] = QtWidgets.QTextEdit()
        self.Gui['terminal'].setReadOnly(True)

        self.Gui['continue'] = QtWidgets.QPushButton('Lancer la mise à jour UM&EEA')
        self.Gui['continue'].setMaximumWidth(300);
        self.Gui['continue'].clicked.connect(self.signal_runInstall)

        self.Gui['quit'] = QtWidgets.QPushButton('Quitter')
        self.Gui['continue'].setMaximumWidth(300);
        self.Gui['quit'].clicked.connect(self.signal_abortInstall)


        self.Gui['footer'] = QtWidgets.QLabel()
        self.Gui['footer'].setTextFormat(Qt.RichText)
        text = '<b>Mise à jour et Installation de Nouveaux Logiciels par le Département EEA</b> v. 1.1 (Apr.2021)'
        text += '<br/><i>Ecrit en Python 3.7/pyQt5 par <A HREF="mailto:mikhael.myara@umontpellier.fr">mikhael.myara@umontpellier.fr</A> et  <A HREF="mailto:benjamin.navarro@umontpellier.fr">benjamin.navarro@umontpellier.fr</A></i><br/> <Université de Montpellier'
        text += '<br/><i>Département EEA - Faculté des Sciences</i><br/></i>Université de Montpellier</i>'
        text = eeaWidgetHTML_Qt(credits=text)
        self.Gui['footer'].setText(text)


        self.Gui['mainL'] = QtWidgets.QVBoxLayout()
        #self.Gui['mainL'].setAlignment(Qt.AlignHCenter)  

        self.Gui['mainL'].addItem(QtGui.QSpacerItem(10, 20, QtGui.QSizePolicy.Minimum))
        self.Gui['mainL'].addWidget(self.Gui['continue'])
        self.Gui['mainL'].addItem(QtGui.QSpacerItem(10, 20, QtGui.QSizePolicy.Minimum))
        self.Gui['mainL'].addWidget(self.Gui['terminal'])
        self.Gui['mainL'].addItem(QtGui.QSpacerItem(10, 20, QtGui.QSizePolicy.Minimum))
        self.Gui['mainL'].addWidget(self.Gui['quit'])
        self.Gui['mainL'].addItem(QtGui.QSpacerItem(10, 20, QtGui.QSizePolicy.Minimum))
        self.Gui['mainL'].addWidget(self.Gui['footer'])

        self.Gui['mainL'].setAlignment(self.Gui['continue'],Qt.AlignCenter)
        self.Gui['mainL'].setAlignment(self.Gui['quit'],Qt.AlignCenter)

        self.setLayout(self.Gui['mainL'])



        self.mutex = QMutex()

    def parse_config(self,filename):
        COMMENT_CHAR = '#'
        OPTION_CHAR =  '='
        options = {}
        f = open(filename)
        for line in f:
            if COMMENT_CHAR in line:
                line, comment = line.split(COMMENT_CHAR, 1)
            if OPTION_CHAR in line:

                option, value = line.split(OPTION_CHAR, 1)
                option = option.strip()
                value = value.strip()
                options[option] = value
        f.close()
        return options



    def getEEAPath(self): # should use env
        try:
            envFile =   os.path.join('/','etc','umeea','default.conf')
            env=self.parse_config(envFile)
            env['UMEEA_PATH']
        except:
            msgBox = QtWidgets.QMessageBox()
            msgBox.setIcon(QtWidgets.QMessageBox.Warning)
            msgBox.setText("Le fichier "+envFile+" semble absent. Il n'est pas possible de fonctionner sans ce fichier,le programme va quitter.")
            msgBox.setWindowTitle("Erreur")
            msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok )
            msgBox.exec()
            exit(1)
        return env['UMEEA_PATH']

    def getProfilePath(self):
        homeDir=QtCore.QStandardPaths.writableLocation(QtCore.QStandardPaths.HomeLocation)
        return os.path.join(homeDir,'.config','umeea')




    def umeeaCommand(self,cmd,extra=[],asynchronous=True,stdOutput=None,finished=None):
        toRun=os.path.join(self.getEEAPath(),"bin",cmd)
        return self.runBashCommand(toRun,extra=extra,check=True,asynchronous=asynchronous,stdOutput=stdOutput,finished=finished)

    def runBashCommand(self,cmd,extra=[],check=False,asynchronous=True,stdOutput=None,finished=None):
        if  os.path.exists(cmd) or check==False:
            self.process = QtCore.QProcess(self)
            self.Gui['terminal'].append("#> "+cmd+" "+" ".join(extra))

            if asynchronous == True:
                if stdOutput == None: stdOutput = self.on_readyReadStandardOutput
                if finished == None : finished  = self.on_finished
                self.process.readyReadStandardOutput.connect(stdOutput)
                self.process.finished.connect(finished)

            self.process.start(cmd,extra)

            if asynchronous == False:
                self.process.waitForFinished()
                return self.on_readyReadStandardOutput()

        else: 
            self.Gui['terminal'].append("#> Commande Introuvable ! : "+cmd+" "+" ".join(extra) +"<#")

        return None



    def signal_runInstall(self):
        if self.mutex.tryLock():
            self.umeeaCommand('umeeaUpdateWorker.sh',asynchronous=True,\
            stdOutput=self.on_readyReadStandardOutput,finished=self.on_finished)
            #self.process = QtCore.QProcess(self)
            #self.process.setProgram("/opt/um")
            #self.process.setArguments([])
            #self.process.start()
            #self.process.readyReadStandardOutput.connect(self.on_readyReadStandardOutput)
            #self.process.finished.connect(self.on_finished)


    @QtCore.pyqtSlot()
    def on_readyReadStandardOutput(self):
        text = self.process.readAllStandardOutput().data().decode()
        self.Gui['terminal'].append(text)

    @QtCore.pyqtSlot()
    def on_finished(self):
        self.Gui['terminal'].append('\n\n# INSTALLATION TERMINEE\n')
        self.mutex.unlock()
    #self.button.setText("Start")


    def signal_abortInstall(self):
        if self.mutex.tryLock():
            app.quit()

    def closeEvent(self,event):
        if self.mutex.tryLock():
            event.accept();
        else:
            event.ignore();

app = QtWidgets.QApplication(sys.argv)

win = infoWindow()
win.setGeometry(QtWidgets.QStyle.alignedRect(Qt.LeftToRight,Qt.AlignCenter,win.size(),app.desktop().availableGeometry()))

win.show()
sys.exit(app.exec())
