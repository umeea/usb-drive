#!/opt/anaconda3/bin/python3

from PyQt5.QtCore import QObject, QThread, pyqtSignal,QTimer,QMutex,Qt
from PyQt5 import QtWidgets,QtCore,QtGui
from waitingspinnerwidget import QtWaitingSpinner


import pyqtgraph as pg
import numpy as np
from datetime import datetime
import time,base64,os,re,sys,base64,tempfile,shutil,tarfile
 

def encodeFile(fileName):
    SVGFileName = fileName
    f=open(SVGFileName,'rb')
    svgdata = f.read()
    encoded = base64.b64encode(svgdata).decode("utf-8")
    f.close()
    return encoded

def logo_Qt(fileName,width=0,height=0):
    fullPathName = '/opt/umeea/media/'+fileName
    png =   encodeFile(fullPathName)
    widthstr=''
    heightstr=''
    if width != 0:
        widthstr='width="{0} "'.format(width)
    if height != 0:
        heightstr='height="{0} "'.format(width)
    return '<img alt="'+fileName+'" {0}{1}src="data:image/png;base64,{2}" />'.format( str(heightstr),str(widthstr),png)



def eeaWidgetHTML_Qt(credits=None):

    logoEEA = logo_Qt('logoEEA-50px.png')
    logoUM = logo_Qt('logoUM-50px.png')
    logoFDS = logo_Qt('logoFDS-50px.png')
    creditsLine =  ""
    if credits is not None:
        creditsLine = "<td width='99%' style='text-align:left;' ><p style='margin-left:15px;font-size:small'>{0}</p></td>".format(credits)

    table = "<hr width='100%'>"+"""<table border='0' width='100%' style='margin: 0px;padding:0px;'>
        <tr>
            <td style='text-align:left !important;white-space: nowrap; padding-right:15px;'>{0}</td>
            <td style='white-space: nowrap;padding-right:15px;'>{1}</td>
            <td style='text-align:right !important:white-space: nowrap;padding-right:15px;'>{2}</td>
            {3}
       </tr>
    </table>""".format(logoEEA,logoUM,logoFDS,creditsLine)
    #return logoEEA
    return table




class infoWindow(QtWidgets.QWidget):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.setWindowTitle('Compte Institutionnel - Université de Montpellier')
        self.setMinimumWidth(700)
        self.setMinimumHeight(520)

        self.Gui={}        

        ### UTILITIES PANEL
        self.Gui['utilitiesL'] = QtWidgets.QVBoxLayout()
        self.Gui['utilities'] =  QtWidgets.QWidget()
        self.Gui['utilities'].setLayout(self.Gui['utilitiesL'])

        self.Gui['accountGroupBoxL'] = QtWidgets.QVBoxLayout()

        self.Gui['accountGroupBox'] = QtWidgets.QGroupBox("Compte Informatique de l'Université")

        self.Gui['accountGroupBox'].setLayout(self.Gui['accountGroupBoxL'])
        self.Gui['spinningWheel']=QtWaitingSpinner(self,True,True)
        self.Gui['accountLabel'] = QtWidgets.QLabel()
        self.Gui['accountLabel'].setTextFormat(Qt.RichText)
        self.Gui['accountLabel'].setWordWrap(True)
        self.Gui['accountAuto'] = QtWidgets.QCheckBox("Monter automatiquement")
        self.Gui['accountAuto'].stateChanged.connect(self.signal_accountAuto)

        self.Gui['accountAutoLabel']= QtWidgets.QLabel()
        self.Gui['accountAutoLabel'].setTextFormat(Qt.RichText)
        self.Gui['accountAutoLabel'].setText("<p><i>Le dossier sera monté automatiquement lors de votre prochain redémarrage. Vous pouvez, en plus, le monter immédiatement avec le bouton 'Monter Maintenant' ci-dessous.</i></p>")
        #self.Gui['accountAutoLabel'].setMaximumWidth(500)
        #self.Gui['accountAutoLabel'].setMaximumWidth(400)
        self.Gui['accountAutoLabel'].setWordWrap(True)
        self.Gui['accountMountNow'] = QtWidgets.QPushButton("Monter Maintenant")
        self.Gui['accountMountNow'].setMaximumWidth(400)
        self.Gui['accountMountNow'].setMinimumWidth(300)
        self.Gui['accountMountNow'].clicked.connect(self.signal_accountMountNow)
        self.Gui['accountMountNowWithWindow'] = QtWidgets.QPushButton("Monter Maintenant \n et ouvrir la fenêtre.")
        self.Gui['accountMountNowWithWindow'].setMaximumWidth(400)
        self.Gui['accountMountNowWithWindow'].setMinimumWidth(300)
        self.Gui['accountMountNowWithWindow'].clicked.connect(self.signal_accountMountNowWidthWindow)
        self.Gui['accountUmountNow'] = QtWidgets.QPushButton("Démonter Maintenant")
        self.Gui['accountUmountNow'].setMaximumWidth(400)
        self.Gui['accountUmountNow'].setMinimumWidth(300)
        self.Gui['accountUmountNow'].clicked.connect(self.signal_accountUmountNow)
        #self.Gui['spinningWheel'].show()
        self.Gui['accountGroupBoxL'].addWidget(self.Gui['accountLabel'])
        self.Gui['accountGroupBoxL'].addSpacing(10)
        self.Gui['accountGroupBoxL'].addWidget(self.Gui['accountAuto'])
        self.Gui['accountGroupBoxL'].setAlignment(self.Gui['accountAuto'],Qt.AlignCenter)
        self.Gui['accountGroupBoxL'].addWidget(self.Gui['accountAutoLabel'])


        self.Gui['accountGroupBoxL'].addSpacing(40)
        self.Gui['accountGroupBoxL'].addWidget(self.Gui['accountMountNow'])
        self.Gui['accountGroupBoxL'].setAlignment(self.Gui['accountMountNow'],Qt.AlignCenter)
        self.Gui['accountGroupBoxL'].addWidget(self.Gui['accountMountNowWithWindow'])
        self.Gui['accountGroupBoxL'].setAlignment(self.Gui['accountMountNowWithWindow'],Qt.AlignCenter)

        self.Gui['accountGroupBoxL'].addWidget(self.Gui['accountUmountNow'])
        self.Gui['accountGroupBoxL'].setAlignment(self.Gui['accountUmountNow'],Qt.AlignCenter)
        self.Gui['accountGroupBoxL'].addStretch()
        self.Gui['accountGroupBoxL'].addWidget(self.Gui['spinningWheel'])

        self.Gui['utilitiesL'].addWidget(self.Gui['accountGroupBox'])
        self.Gui['utilitiesL'].addSpacing(10)


        ### CONFIGURATION PANEL

        self.Gui['configL'] = QtWidgets.QVBoxLayout() 
        self.Gui['config'] =  QtWidgets.QWidget()
        self.Gui['config'].setLayout(self.Gui['configL'])

        self.Gui['userinfoGroupBoxL'] = QtWidgets.QFormLayout()
        self.Gui['userinfoGroupBox'] = QtWidgets.QGroupBox("Informations de votre compte :")       
        self.Gui['userinfoGroupBox'].setLayout(self.Gui['userinfoGroupBoxL'])
        self.Gui['USER_LAST_NAME'] = QtWidgets.QLineEdit()
        self.Gui['userinfoGroupBoxL'].addRow('Nom :',self.Gui['USER_LAST_NAME'])
        self.Gui['USER_FIRST_NAME'] = QtWidgets.QLineEdit()
        self.Gui['userinfoGroupBoxL'].addRow('Prénom :',self.Gui['USER_FIRST_NAME'])
        self.Gui['userEmail'] = QtWidgets.QLineEdit()
        self.Gui['userinfoGroupBoxL'].addRow('Email Institutionnel :',self.Gui['userEmail'])
        self.Gui['userPass'] = QtWidgets.QLineEdit()
        self.Gui['userinfoGroupBoxL'].addRow('Mot de passe :',self.Gui['userPass'])
        self.Gui['userPassShow'] = QtWidgets.QCheckBox('Afficher le mot de passe en clair')
        self.Gui['userPassShow'].stateChanged.connect(self.signal_userPassShow)
        self.Gui['userinfoGroupBoxL'].addWidget(self.Gui['userPassShow'])
        self.Gui['userCheck'] = QtWidgets.QPushButton('Sauver et Tester')
        self.Gui['userCheck'].clicked.connect(self.signal_checkConfiguration)
        self.Gui['userCheck'].setMinimumWidth(300)
        self.Gui['userinfoGroupBoxL'].addRow(self.Gui['userCheck'])     
        self.Gui['userinfoGroupBoxL'].setAlignment(self.Gui['userCheck'],Qt.AlignCenter)  
        self.Gui['userCheckLabel']=QtWidgets.QLabel();
        self.Gui['userCheckLabel'].setTextFormat(Qt.RichText)
        self.Gui['userCheckLabel'].setText("L'email institutionnel est de la forme nom.prenom@etu.umontpellier.fr")
        self.Gui['userCheckLabel'].setWordWrap(True)
        self.Gui['userinfoGroupBoxL'].addRow(self.Gui['userCheckLabel'])     

        self.Gui['actionGroupBoxL'] = QtWidgets.QVBoxLayout()
        self.Gui['actionGroupBox'] = QtWidgets.QGroupBox("Ce qui sera configuré ... :")       
        self.Gui['actionGroupBox'].setLayout(self.Gui['actionGroupBoxL'])

        self.Gui['mailCfg'] = QtWidgets.QCheckBox('Configurer Thunderbird (Logiciel de Mail). Ecrase votre ancienne configuration !')
        self.Gui['mailCfg'].setChecked(True)
        self.Gui['actionGroupBoxL'].addWidget(self.Gui['mailCfg'])
        self.Gui['x2goCfg'] = QtWidgets.QCheckBox("Configurer X2Go (Connexion aux ordinateurs de l'université').")
        self.Gui['x2goCfg'].setChecked(True)
        self.Gui['actionGroupBoxL'].addWidget(self.Gui['x2goCfg'])
        self.Gui['SSHFSCfg'] = QtWidgets.QCheckBox("Configurer SSHFS (Accès direct à vos fichiers à l'université).")
        self.Gui['SSHFSCfg'].setChecked(True)
        self.Gui['actionGroupBoxL'].addWidget(self.Gui['SSHFSCfg'])
        self.Gui['SSHFSRemoveCfg'] = QtWidgets.QCheckBox("Effacer la configuration SSHFS existante (Peut supprimer l'accès automatique à d'autres comptes SSH/SSHFS)")
        self.Gui['SSHFSRemoveCfg'].setChecked(False)
        self.Gui['actionGroupBoxL'].addWidget(self.Gui['SSHFSRemoveCfg'])
        self.Gui['runConfiguration'] = QtWidgets.QPushButton('Lancer la Configuration')
        self.Gui['runConfiguration'].clicked.connect(self.signal_runConfiguration)
        self.Gui['runConfiguration'].setMinimumWidth(300)
        self.Gui['actionGroupBoxL'].addWidget(self.Gui['runConfiguration'])
        self.Gui['actionGroupBoxL'].setAlignment(self.Gui['runConfiguration'],Qt.AlignCenter)  

        self.Gui['configL'].addWidget(self.Gui['userinfoGroupBox'])       
        self.Gui['configL'].addWidget(self.Gui['actionGroupBox'])       

        ### TERMINAL PANEL
        self.Gui['terminal'] = QtWidgets.QTextEdit()
        self.Gui['terminal'].setReadOnly(True)

        self.Gui['coreTabs'] = QtWidgets.QTabWidget()
        self.Gui['coreTabs'].addTab(self.Gui['utilities'], "Utilitaires")
        self.Gui['coreTabs'].addTab(self.Gui['config'], "Configuration")
        self.Gui['coreTabs'].addTab(self.Gui['terminal'], "Log")
        self.Gui['coreTabs'].currentChanged.connect(self.signal_coreTabChanged)

        self.Gui['footer'] = QtWidgets.QLabel()
        self.Gui['footer'].setTextFormat(Qt.RichText)
        text = '<b>Compte Institutionnel - Université de Montpellier</b> v. 1.1 (Apr.2021)'
        text += '<br/><i>Ecrit en Python 3.7/pyQt5 par <A HREF="mailto:mikhael.myara@umontpellier.fr">mikhael.myara@umontpellier.fr</A>'
        text += '<br/><i>Département EEA - Faculté des Sciences</i><br/></i>Université de Montpellier</i>'
        text = eeaWidgetHTML_Qt(credits=text)
        self.Gui['footer'].setText(text)

        self.Gui['mainL'] = QtWidgets.QGridLayout()
        self.Gui['mainL'].addWidget(self.Gui['coreTabs'],0,0,1,2)
#        self.Gui['mainL'].addWidget(self.Gui['abort'],1,0)
#        self.Gui['mainL'].addWidget(self.Gui['continue'],1,1)
        self.Gui['mainL'].addWidget(self.Gui['footer'],2,0,1,2)

        self.setLayout(self.Gui['mainL'])

        self.mutex = QMutex()


        self.configTestPassed = False

        self.Gui['coreTabs'].setCurrentIndex(1)
        self.signal_coreTabChanged()


    def umeeaCommand(self,cmd,extra=[],asynchronous=True,stdOutput=None,finished=None):
        toRun=os.path.join(self.getEEAPath(),"bin",cmd)
        return self.runBashCommand(toRun,extra=extra,check=True,asynchronous=asynchronous,stdOutput=stdOutput,finished=finished)

    def runBashCommand(self,cmd,extra=[],check=False,asynchronous=True,stdOutput=None,finished=None):
        if  os.path.exists(cmd) or check==False:
            self.process = QtCore.QProcess(self)
            self.Gui['terminal'].append("#> "+cmd+" "+" ".join(extra))

            if asynchronous == True:
                if stdOutput == None: stdOutput = self.on_readyReadStandardOutput
                if finished == None : finished  = self.on_finished
                self.process.readyReadStandardOutput.connect(stdOutput)
                self.process.finished.connect(finished)

            self.process.start(cmd,extra)

            if asynchronous == False:
                self.process.waitForFinished()
                return self.on_readyReadStandardOutput()

        else: 
            self.Gui['terminal'].append("#> Commande Introuvable ! : "+cmd+" "+" ".join(extra) +"<#")

        return None

    def signal_accountMountNowWidthWindow(self):
        if self.mutex.tryLock():
            self.umeeaCommand("umeeaMountUniversityAccount.sh",extra=['-x'],asynchronous=False)
            self.mutex.unlock()

    def signal_accountMountNow(self):
        if self.mutex.tryLock():
            self.umeeaCommand("umeeaMountUniversityAccount.sh",asynchronous=False)
            self.mutex.unlock()

    def signal_accountUmountNow(self):
        if self.mutex.tryLock():
            self.umeeaCommand("umeeaUmountUniversityAccount.sh",asynchronous=False)
            self.mutex.unlock()

    def signal_accountAuto(self):
        homeAutoMountAccount=os.path.join(self.getProfilePath(),'autoMountSSHFS')
        if self.Gui['accountAuto'].isChecked():
            os.makedirs(self.getProfilePath(), exist_ok=True)
            open(homeAutoMountAccount, 'w').close()
        else:
            if os.path.exists(homeAutoMountAccount): os.remove(homeAutoMountAccount)
 
    def getEEAPath(self): # should use env
        try:
            envFile =   os.path.join('/','etc','umeea','default.conf')
            env=self.parse_config(envFile)
            env['UMEEA_PATH']
        except:
            msgBox = QtWidgets.QMessageBox()
            msgBox.setIcon(QtWidgets.QMessageBox.Warning)
            msgBox.setText("Le fichier "+envFile+" semble absent. Il n'est pas possible de fonctionner sans ce fichier,le programme va quitter.")
            msgBox.setWindowTitle("Erreur")
            msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok )
            msgBox.exec()
            exit(1)
        return env['UMEEA_PATH']

    def getProfilePath(self):
        homeDir=QtCore.QStandardPaths.writableLocation(QtCore.QStandardPaths.HomeLocation)
        return os.path.join(homeDir,'.config','umeea')



    @QtCore.pyqtSlot()
    def signal_coreTabChanged(self):
        if self.mutex.tryLock():
            if self.Gui['coreTabs'].currentIndex()==0: # utilities tab selected
                enablableWidgets =['accountAuto','accountAutoLabel','accountMountNow','accountUmountNow','accountMountNowWithWindow'];

                text="<p style='margin:2'><b>Cette section concerne les fichiers de votre compte institutionnel.</b></p>"
                for widget in enablableWidgets:  self.Gui[widget].setEnabled(False)

                #self.repaint()
                self.Gui['spinningWheel'].start()
                self.umeeaCommand('umeeaSshChecker.sh',asynchronous=True,\
                stdOutput=self.on_SshfsCmdReadStandardOutput,finished=self.on_SshfsCmdFinished)
                
                # the update itself is handled by self.on_SshfsCmdReadStandardOutput
                self.Gui['accountLabel'].setText(text)   


            elif self.Gui['coreTabs'].currentIndex()==1: # configuration tab selected      
                if self.configTestPassed == False :         
                    self.Gui['actionGroupBox'].setEnabled(False)

                try:
                    profileFile=os.path.join(self.getProfilePath(),'profile.conf')
                    userData=self.parse_config(profileFile)
                except:
                    userData = {} 
            
                for key in ['USER_FIRST_NAME','USER_LAST_NAME']:
                    if key in userData.keys():
                        self.Gui[key].setText(userData[key])

                if 'SSH_ACCOUNT'  in  userData.keys():
                    email=self.getEmailFromAccount(userData['SSH_ACCOUNT'])
                    self.Gui['userEmail'].setText(email)

                self.Gui['userPassShow'].setChecked(False)
                self.signal_userPassShow()
                

            self.mutex.unlock()



    def signal_userPassShow(self):
        if self.Gui['userPassShow'].isChecked():
            self.Gui['userPass'].setEchoMode(QtWidgets.QLineEdit.Normal) 
        else:
            self.Gui['userPass'].setEchoMode(QtWidgets.QLineEdit.Password)            

    def signal_checkConfiguration(self):

        userData = self.buildUserDataFromGUI()
        if self.saveProfileFile(userData) == True:

            # Should test here the configuration with umeeaSshChecker.sh within host and pass parameters given
            # test fails :
            # self.configTestPassed = False
            # self.Gui['actionGroupBox'].setEnabled(False)
            
            # test pass :

            self.configTestPassed = True
            self.Gui['actionGroupBox'].setEnabled(True)
            self.Gui['userCheckLabel'].setText("<p style='color:darkgreen;font-weight:bold'>Ces informations sont bien liées à un compte institutionnel de l'Université de Montpellier' !</p>")


    def buildUserDataFromGUI(self):
        userData = {} 
        userData['SSH_ACCOUNT']=self.Gui['userEmail'].text().strip()+'@x2go.umontpellier.fr'

        for key in ['USER_FIRST_NAME','USER_LAST_NAME']:
            userData[key]=self.Gui[key].text().strip()

        userData['userPass']=self.Gui['userPass'].text().strip()

        if 'LOCAL_SSHFS_FOLDER' not in userData.keys():
            userData['LOCAL_SSHFS_FOLDER']='UniversityAccount'
        return userData


    def saveProfileFile(self,userData):    
        profileConf="""#!/bin/bash
# Generated by umeeaAssistant.py

# the full SSH account :
SSH_ACCOUNT={0}
# the local folder in which the account will be mounted with sshfs. Is relative to home.
LOCAL_SSHFS_FOLDER=UniversityAccount
# User's first name
USER_FIRST_NAME={1}
# User's last name
USER_LAST_NAME={2}
""".format(userData['SSH_ACCOUNT'],userData['USER_FIRST_NAME'].strip(),userData['USER_LAST_NAME'].strip())
        try:
            os.makedirs(self.getProfilePath(), exist_ok=True)
            profileFile=open(os.path.join(self.getProfilePath(),'profile.conf'),'w')
            profileFile.write(profileConf)
            profileFile.close()
            return True
        except:
            msgBox = QtWidgets.QMessageBox()
            msgBox.setIcon(QtWidgets.QMessageBox.Warning)
            msgBox.setText("Le fichier de configuration n'a pas pu être sauvé !'")
            msgBox.setWindowTitle("Erreur")
            msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok )
            msgBox.exec()
            return False

    def signal_runConfiguration(self):
        userData = self.buildUserDataFromGUI()

        if self.Gui['mailCfg'].isChecked():
            self.configureMail(userData)

        if self.Gui['x2goCfg'].isChecked():
            self.configureX2Go(userData)

        if self.Gui['SSHFSCfg'].isChecked():
            if self.Gui['SSHFSRemoveCfg'].isChecked():  
                  eraseExistingConfiguration=True
            else: eraseExistingConfiguration=False
            
            self.configureSSHFS(userData,eraseExistingConfiguration)

    def getEmailFromAccount(self,account):
        regex = re.compile(r"(^.+)@(.+)$");
        parsed = regex.findall(account)[0]
        return parsed[0]

    def configureMail(self,userData):
        homeDir=QtCore.QStandardPaths.writableLocation(QtCore.QStandardPaths.HomeLocation)

        #workFile = os.path.join(workpath,'sessions')
        templatePackageName = os.path.join(self.getEEAPath(),'var','umeeaAssistant',\
        'thunderbird.template.tar.gz')
        thunderbirdPref =  os.path.join(homeDir,'.thunderbird')
        if os.path.exists(thunderbirdPref) and os.path.isdir(thunderbirdPref):
            shutil.rmtree(thunderbirdPref)

        templatePkg = tarfile.open(templatePackageName)
        templatePkg.extractall(homeDir)
        templatePkg.close()

        templateFile = os.path.join(thunderbirdPref,'ezvdyi48.default-release','prefs.js.template')
        prefFile = os.path.join(thunderbirdPref,'ezvdyi48.default-release','prefs.js')        
        email=self.getEmailFromAccount(userData['SSH_ACCOUNT'])
        keywords = {'imap_user':'myara','imap_server':'imap.ies.univ-montp2.fr',\
        'first_name':userData['USER_FIRST_NAME'],'last_name':userData['USER_LAST_NAME'],\
        'email':email,'home_dir':homeDir,'smtp_server':'smtp.ies.univ-montp2.fr',\
        'smtp_user':'myara' }        
        self.createPrefByTemplate(templateFile,prefFile,keywords)

    def configureX2Go(self,userData):
        homeDir=QtCore.QStandardPaths.writableLocation(QtCore.QStandardPaths.HomeLocation)
        workpath= os.path.join(homeDir,'.x2goclient')
        if not os.path.exists(workpath):
            os.mkdir(workpath )
        workFile = os.path.join(workpath,'sessions')
        template = os.path.join(self.getEEAPath(),'var','umeeaAssistant','x2go.session.template')
        email=self.getEmailFromAccount(userData['SSH_ACCOUNT'])
        keywords = {'um_user_email':email}
        self.createPrefByTemplate(template,workFile,keywords)
            
    def configureSSHFS(self,userData,erasePrev):
        extraParams=['-P',userData['userPass'] ]
        if erasePrev == True:
            extraParams.append('-C')
        # perhaps add a -C optionnal from GUI
        self.umeeaCommand("umeeaSshAccountNoPass.sh",extra=extraParams,asynchronous=False)

    @QtCore.pyqtSlot()
    def on_SshfsCmdReadStandardOutput(self):
        if not hasattr(self,'echo'):
            self.echo ="";     
        newEcho = self.on_readyReadStandardOutput()
        self.repaint()
        self.echo+=newEcho

    @QtCore.pyqtSlot()
    def on_SshfsCmdFinished(self):
        if not hasattr(self,'echo'):
            self.echo =""; 

        echo=self.echo.replace('\n','').strip()

        try:
            enablableWidgets =['accountAuto','accountAutoLabel','accountMountNow','accountUmountNow','accountMountNowWithWindow'];
            text=self.Gui['accountLabel'].text()

            homeProfile=os.path.join(self.getProfilePath(),'profile.conf')
            profile = self.parse_config(homeProfile)
            for kw in ("SSH_ACCOUNT","LOCAL_SSHFS_FOLDER"):
                if kw not in profile: raise ValueError('Missing Key !')

            #text="<b>Cette section concerne la configuration de votre compte institutionnel.</b>"


            knownAnswer = True
            if echo=='SSH_OK':
                text+="<p style='color:darkgreen;margin:2'>Votre compte <b>'%s'</b> est configuré, la connexion a pu être établie.</p>"%profile['SSH_ACCOUNT']
                text+="<p style='margin:2'>Vous pouvez accéder aux fichiers de votre compte institutionnel via un répertoire sur votre Bureau <i style='color:green;margin:2'>(ici <b>'%s'</b>)</i>. Plusieurs actions vous sont proposées ci-dessous :</p>"%profile['LOCAL_SSHFS_FOLDER']  ;
                for widget in enablableWidgets: self.Gui[widget].setEnabled(True)
            elif echo=='SSH_NOK':
                text+="<p style='color:orangered;margin:2'>Votre compte <b>'%s'</b> semble bien configuré."%profile['SSH_ACCOUNT']
                text+=" Malgré tout la connexion à votre compte est impossible pour le moment. Cela signifie que: <ul style='margin:2'><li style='display: flex;'>soit les informations de votre compte ne sont pas les bonnes,</li><li> soit votre connexion réseau ne fonctionne pas ou filtre la connexion aux serveurs de l'université,</li><li> ou encore les serveurs de l'université ne soit pas joignables.</li> </p> "
                for widget in enablableWidgets:  self.Gui[widget].setEnabled(False)   
            else:
                 knownAnswer=False       
        except:
                 knownAnswer=False       

        if  knownAnswer==False :     
            text+="<p style='color:darkred;margin:2'><b>Votre compte n'est pas ou est mal configuré.</b></p>"
            text+="<p style='margin:2'>Essayez de le reconfigurer dans l'onglet '<i>Configuration</i>' de ce logiciel. Si vous pensez que vous ne disposez pas des bonnes informations (par exemple parce que vous n'êtes pas inscrit), merci de contacter l'administration de l'université, </p>"
            for widget in enablableWidgets:  self.Gui[widget].setEnabled(False)


        self.Gui['accountLabel'].setText(text)   

        # Check auto account activated or not
        homeAutoMountAccount=os.path.join(self.getProfilePath(),'autoMountSSHFS')
        if os.path.exists(homeAutoMountAccount): 
            self.Gui['accountAuto'].setChecked(True)
        else:
            self.Gui['accountAuto'].setChecked(False)


        self.echo ="";           
        self.on_finished()
        self.Gui['spinningWheel'].stop()

    @QtCore.pyqtSlot()
    def on_readyReadStandardOutput(self):
        text = self.process.readAllStandardOutput().data().decode()
        text+= self.process.readAllStandardError().data().decode()
        self.Gui['terminal'].append(text)
        return text

    @QtCore.pyqtSlot()
    def on_finished(self):
        self.Gui['terminal'].append('\n#> Commande Terminée <#\n\n')
 


    def closeEvent(self,event):
        if self.mutex.tryLock():
            event.accept();
        else:
            event.ignore();

    def parse_config(self,filename):
        COMMENT_CHAR = '#'
        OPTION_CHAR =  '='
        options = {}
        f = open(filename)
        for line in f:
            if COMMENT_CHAR in line:
                line, comment = line.split(COMMENT_CHAR, 1)
            if OPTION_CHAR in line:

                option, value = line.split(OPTION_CHAR, 1)
                option = option.strip()
                value = value.strip()
                options[option] = value
        f.close()
        return options

    def createPrefByTemplate(self,templateName,outName,knownKeywords):
        theTemplateFile = open(templateName, 'r')
        theOutputFile = open(outName, 'w')


        for line in theTemplateFile :       
            newline = line
            for kw,value in knownKeywords.items():
                newline=re.sub("(<@@"+kw+"@@>)",value,newline)
            theOutputFile.write(newline)        



app = QtWidgets.QApplication(sys.argv)

win = infoWindow()
win.setGeometry(QtWidgets.QStyle.alignedRect(Qt.LeftToRight,Qt.AlignCenter,win.size(),app.desktop().availableGeometry()))

win.show()
sys.exit(app.exec())
