#!/bin/bash
UMEEA_PATH="/opt/umeea"
source /etc/umeea/default.conf 2> /dev/null

AUTO_FILE="$HOME/.config/umeea/autoMountSSHFS"
if test -f "$AUTO_FILE"; then
	$UMEEA_PATH/bin/umeeaMountUniversityAccount.sh
else
        $UMEEA_PATH/bin/umeeaUmountUniversityAccount.sh
fi

