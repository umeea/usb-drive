#!/bin/bash
source ../../utilities.sh

banner "BUILDING EEA DEPT OS ADMINISTRATION PACKAGE"

WORKDIR=$(preparePackage)
echo $WORKDIR


# policy to allow root mode for update/install app
mkdir -p $WORKDIR/usr/share/polkit-1/actions/
cp  *.policy $WORKDIR/usr/share/polkit-1/actions/

# add auto mount feature for SSHFS folder
#UMEEA_PATH="/opt/umeea"
#ls ./umeea/etc/umeea/default.conf
#source ./umeea/etc/umeea/default.conf 2> /dev/null
#mkdir -p $WORKDIR/opt/umeea/etc/profile.d/ 
#cp "./umeea/bin/umeeaAutoMountSSHFS.sh" "$WORKDIR/opt/umeea/etc/profile.d/"
#chmod 644 $WORKDIR/opt/umeea/etc/profile.d/umeeaAutoMountSSHFS.sh

finalizePackage $WORKDIR
