#!/bin/bash
source ../../utilities.sh

banner "BUILDING ZOOM PACKAGE"

WORKDIR=$(preparePackage)
echo $WORKDIR
rm zoom_amd64.deb.*
wget https://zoom.us/client/latest/zoom_amd64.deb


finalizePackage $WORKDIR
