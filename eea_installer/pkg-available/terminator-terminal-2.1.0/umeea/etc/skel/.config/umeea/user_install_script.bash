# Check if we're running in a terminal with stdin attached
if [ -t 0 ]; then
    echo "This is the first time you open a terminal. Some optional software and configuration and be installed. Please answer y(es) or n(o) to the following questions."
    read -p "Do you want to install common VSCodium extensions? " -n 1 -r
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        echo ""
        for ext in twxs.cmake ms-vscode.cmake-tools llvm-vs-code-extensions.vscode-clangd webfreak.debug cschlosser.doxdocgen eamodio.gitlens KnisterPeter.vscode-commitizen ms-vscode.atom-keybindings MS-vsliveshare.vsliveshare ms-vscode-remote.vscode-remote-extensionpack redhat.vscode-yaml DotJoshJohnson.xml;
        do
            codium --install-extension $ext; 
        done
    fi
    echo ""
    echo "We're done, enjoy!"
    return 0
else
    return 1
fi