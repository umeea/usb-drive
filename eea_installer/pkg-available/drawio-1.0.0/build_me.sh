#!/bin/bash
source ../../utilities.sh

banner "BUILDING DRAWIO PACKAGE"

WORKDIR=$(preparePackage)
PKGNAME=drawio-amd64-14.5.1.deb
echo $WORKDIR
rm *.deb

wget https://github.com/jgraph/drawio-desktop/releases/download/v14.5.1/$PKGNAME


finalizePackage $WORKDIR
