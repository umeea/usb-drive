#!/bin/bash
source ../../utilities.sh

banner "BUILDING COPPELIASIM PACKAGE"


WORKDIR=$(preparePackage)
echo $WORKDIR

rm CoppeliaSim_Edu_V4_2_0_Ubuntu20_04.tar.*

wget https://coppeliarobotics.com/files/CoppeliaSim_Edu_V4_2_0_Ubuntu20_04.tar.xz
tar xf CoppeliaSim_Edu_V4_2_0_Ubuntu20_04.tar.xz
mv CoppeliaSim_Edu_V4_2_0_Ubuntu20_04 $WORKDIR/opt/coppeliaSim

finalizePackage $WORKDIR
