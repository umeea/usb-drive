#!/bin/bash
source ../../utilities.sh

banner "BUILDING INSTRU PACKAGE"

WORKDIR=$(preparePackage)
echo $WORKDIR

git clone https://gitlab.com/umeea/instrumentation.git
mkdir -p  $WORKDIR/umeea/bin/
cp ./instrumentation/BodeEEA/*  $WORKDIR/umeea/bin/
cp ./instrumentation/ScopeEEA/*  $WORKDIR/umeea/bin/


finalizePackage $WORKDIR
