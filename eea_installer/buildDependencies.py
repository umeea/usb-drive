#!/usr/bin/python3
import sys
if len(sys.argv) != 4:
  print("Error. Good Syntax : buildDependencies templateFile configFile autoDep\n")
  exit()


if int(sys.argv[3]) == 0:
  deps=[]
else:
  deps=['umeea-base (>=1.0.0)']
  
try:
  templateName = sys.argv[1]
  updateConfigName = sys.argv[2]
  template = open(templateName,'r')
  Lines = template.readlines()
except:
  template = None
  Lines=[]

config = open(updateConfigName,'a')  

for line in Lines :
  if line[0]!='#':
    theLine = line.strip()
    if len(theLine) !=0:
      deps.append(theLine)

if len(deps) > 0:
  config.write('Depends: '+','.join(deps)+'\n')

config.close()
if template != None:
  template.close()





