#!/bin/bash

./prepareRepo.sh
cp KEY.gpg  ./repo/
cp umeea-1.15_amd64.deb ./repo/amd64

./finalizeRepo.sh

echo apres avoir uploadé dans le dossier ppa (par exemple) sur le site www.eea.univ-montp2.fr, 
echo le depot sera disponible pour les clients s'ils créent un fichier xxx.list dont le contenu est uniquement :
echo "deb http://www.eea.univ-montp2.fr/ppa/ /" 
echo dans  /etc/apt/sources.list.d/
echo puis ils doivent télécharger la clé :
echo "wget -q -O - http://${HOST}/repo/KEY.gpg | apt-key add -"
echo "et l'opération sera validée si l'opération ci-dessous fonctionne"
echo "apt update"
