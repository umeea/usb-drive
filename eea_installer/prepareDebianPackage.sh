#!/bin/bash

if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters :"
    echo " - first argument must be package name (ex : umeea),"
    echo " - second argument must be version number (ex: 1.01),"
    echo " - architecture (ex : amd64)"
    echo
    exit
fi
PKGNAME=$1 #umeea
VERSION=$2 #1.0.1
ARCHITECTURE=$3 #amd64


### Generate version executable (for check purposes)
VERSIONEXE=version-${PKGNAME}
cat > ${VERSIONEXE}.cc << EOF
#include <iostream>
int main() {
    using namespace std;
    cout << "${PKGNAME} v. ${VERSION}\n";
    return 0;
}
EOF

g++ ${VERSIONEXE}.cc -o ${VERSIONEXE}

mkdir -p ./${PKGNAME}/opt/umeea/bin
mv ${VERSIONEXE} ./${PKGNAME}/opt/umeea/bin/
rm ${VERSIONEXE}.cc

### Generate package structure
mkdir -p ./${PKGNAME}/DEBIAN

cat > ./${PKGNAME}/DEBIAN/control << EOF
Package: $PKGNAME
Version: $VERSION
Section: custom
Priority: optional
Architecture: $ARCHITECTURE
Essential: no
Installed-Size: 1024
Maintainer: www.eea.univ-montp2.fr
Description: Software package for EEA department of Montpellier University
EOF

