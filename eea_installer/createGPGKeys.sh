#/bin/bash

KEYNAME=umeea
EMAIL=${KEYNAME}@eea.univ-montp2.fr

TMPHOME=./GPGHome
GPGCONF=$TMPHOME/.gnupg

echo "removing previous gpg data in $TMPHOME"
rm -rf  $TMPHOME
rm KEY.gpg

mkdir -p $GPGCONF
touch $GPGCONF/gpg.conf

echo "cert-digest-algo SHA256" >> $GPGCONF/gpg.conf
echo "digest-algo SHA256" >> $GPGCONF/gpg.conf


cat > $KEYNAME.batch <<EOF
 %echo Generating a standard key
 Key-Type: RSA
 Key-Length: 4096
 Subkey-Length: 4096
 Name-Real: ${KEYNAME}
 Name-Email: ${EMAIL}
 Expire-Date: 0
 %echo done
EOF

gpg --homedir localHome --pinentry-mode=loopback --passphrase  "" --batch --full-generate-key ${KEYNAME}.batch

echo "check key : "
gpg --homedir localHome --list-keys ${KEYNAME}

echo "export public key as KEY.gpg :"
gpg --homedir localHome --output ./KEY.gpg --armor --export ${KEYNAME}

