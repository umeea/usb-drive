#!/bin/bash




installEtcItem()
{
  if [ "$#" -ne 1 ]; then
  	echo "Please give workdir as parameters !"
  	exit
  fi


WORKDIR=$1
if ls ./umeea/etc/* &> /dev/null; then
	for item  in ./umeea/etc/* ;  do
		ITEMNAME=/opt/umeea/etc/$(basename ${item})
		echo "cp -R "${ITEMNAME}" /etc/ "  >> ${WORKDIR}/DEBIAN/postinst 
	done
	echo "# UPDATED $SCRIPT_DIR/postinst FOR /etc"	
fi
}


installDesktopItem() 
{
  if [ "$#" -ne 3 ]; then
  	echo "Please give kind, extension and workdir as parameters !"
  	exit
  fi

KIND=$1
FILTER=$2
WORKDIR=$3
   if ls ./desktop/${FILTER} &> /dev/null; then
    	echo "mkdir -p /opt/umeea/usr/share/"${KIND}/ >> ${WORKDIR}/DEBIAN/postinst
    	echo "mkdir -p /opt/umeea/usr/share/"${KIND}-backup/ >> ${WORKDIR}/DEBIAN/postinst    	
		for item  in ./desktop/${FILTER}
		do
		   FILE=/usr/share/applications/$(basename ${item})
		   echo "if [  -f "${FILE}" ]; then" >> ${WORKDIR}/DEBIAN/postinst 
		   echo "    mv "${FILE} "/opt/umeea/usr/share/"${KIND}-backup/    >> ${WORKDIR}/DEBIAN/postinst 
		   echo "fi" >> ${WORKDIR}/DEBIAN/postinst 
		   echo "mv /opt/umeea/usr/share/"${KIND}/$(basename ${item}) /usr/share/${KIND}/   >> ${WORKDIR}/DEBIAN/postinst 		   
		done
	echo "# UPDATED $SCRIPT_DIR/postinst FOR "$KIND
	fi
}

installDesktop() {
  if [ "$#" -ne 1 ]; then
  	echo "Please give debian package working directory as parameter !"
  	exit
  fi
  
  	
  if [ -d "./desktop" ]
  then
    
    
    # insert our own icons, desktop and so on in the package
  	mkdir -p $WORKDIR/opt/umeea/usr/share/applications/
    cp ./desktop/*.desktop $WORKDIR/opt/umeea/usr/share/applications/ 2>/dev/null
    mkdir -p $WORKDIR/opt/umeea/usr/share/icons/
    cp ./desktop/*.png $WORKDIR/opt/umeea/usr/share/icons/  2>/dev/null
    mkdir -p $WORKDIR/opt/umeea/usr/share/desktop-directories/
    cp ./desktop/*.directory $WORKDIR/opt/umeea/usr/share/desktop-directories/  2>/dev/null    
        
    
    # backup old desktop, icon and desktop-directories :
    # update preinst/postinst scripts      
    echo -e "\n# Desktop management\n"    >> $WORKDIR/DEBIAN/postinst
    installDesktopItem applications *.desktop $WORKDIR
    installDesktopItem icons *.png $WORKDIR
    installDesktopItem desktop-directories *.directory $WORKDIR   

    
  fi

}


bigbanner()
{

BIWhite='\033[1;97m' 
NC='\033[0m' # No Color
  echo "+------------------------------------------+"
  echo "|  "$(date)                   
  echo "|                                          |"
  echo -e "| ${BIWhite}$@${NC}"
  echo "+------------------------------------------+"
}


banner()
{
BIGreen='\033[1;92m' 
NC='\033[0m' # No Color
  echo -e "# ${BIGreen}$@${NC}"
  echo
}


errorbanner()
{
BIGreen='\033[1;91m' 
NC='\033[0m' # No Color
  echo -e "# ${BIGreen}$@${NC}"
  echo
}

getScriptPath()
{
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
echo $SCRIPTPATH
}

getUtilitiesPath()
{
if [[ "$BASH_SOURCE" = /* ]]; then
	echo $(dirname $BASH_SOURCE)
else 
	echo "$(pwd)/$(dirname $BASH_SOURCE)"
fi
}

getPackageName()
{
# should detect errors here
PYSCRIPT='import re;x=re.search("(.*)-(\d+.\d+.\d+)$","'$1'");print(x[1])'
RUNPY="python3 -c '${PYSCRIPT}'"
NAME=$(eval "$RUNPY")
echo $NAME
}


getVersion()
{
PYSCRIPT='import re;x=re.search("(.*)-(\d+.\d+.\d+)$","'$1'");print(x[2])'
RUNPY="python3 -c '${PYSCRIPT}'"
VERSION=$(eval "$RUNPY")
echo $VERSION
}



preparePackage()
{

LOCALDIR=$(pwd -P)

SCRIPT_DIR=$(getScriptPath)
rm $SCRIPT_DIR/umeea-*.deb 2> /dev/null
PKG_FULL_NAME=$(basename ${SCRIPT_DIR})


PKGNAME=umeea-$(getPackageName $PKG_FULL_NAME)
VERSION=$(getVersion $PKG_FULL_NAME)

UPATH=$(getUtilitiesPath)
WORKDIR=$(mktemp -d)
cd $WORKDIR
${UPATH}/prepareDebianPackage.sh $PKGNAME $VERSION amd64
cd $LOCALDIR
echo $WORKDIR/$PKGNAME
}

prepareDEBIANscript()
{
  if [ "$#" -ne 2 ]; then
  	echo "prepareDEBIANscripts :"
  	echo "give :"
  	echo " - WORKDIR as first parameter"  	
  	echo " - DEBIAN script second as parameter (should be preinst, postinst and so on)"  
  	exit
  fi
WORKDIR=$1
DEBSCRIPT=$2

echo "#!/bin/bash" >> $WORKDIR/DEBIAN/$DEBSCRIPT
echo "# CREATED $WORKDIR/DEBIAN/$DEBSCRIPT"


#if [ -f "$SCRIPT_DIR/$DEBSCRIPT" ] ; then
#	cp $SCRIPT_DIR/$DEBSCRIPT $WORKDIR/DEBIAN/$DEBSCRIPT
#	echo "# COPIED $SCRIPT_DIR/$DEBSCRIPT"

#else
#    	echo "#!/bin/bash" >> $WORKDIR/DEBIAN/$DEBSCRIPT
#	echo "# CREATED $SCRIPT_DIR/$DEBSCRIPT"
#fi
chmod 0755 $WORKDIR/DEBIAN/$DEBSCRIPT

}

finalizeDEBIANscript()
{
  if [ "$#" -ne 3 ]; then
  	echo "finalizeDEBIANscript :"
  	echo "give :"
  	echo " - SCRIPT_DIR as first parameter"
  	echo " - WORKDIR as second parameter"  	
  	echo " - DEBIAN script name as parameter (should be preinst, postinst and so on)"  
  	exit
  fi
SCRIPT_DIR=$1
WORKDIR=$2
DEBSCRIPT=$3



if [ -f "$SCRIPT_DIR/$DEBSCRIPT" ] ; then
	echo "" >> $WORKDIR/DEBIAN/$DEBSCRIPT
	echo "# Extra code"  >> $WORKDIR/DEBIAN/$DEBSCRIPT
	cat $SCRIPT_DIR/$DEBSCRIPT >> $WORKDIR/DEBIAN/$DEBSCRIPT
	echo "# UPDATED WITH $SCRIPT_DIR/$DEBSCRIPT"
fi



}



finalizePackage()
{

  if [ "$#" -ne 1 ]; then
  	echo "finalizePackage :"
  	echo "Please give debian package working directory as parameter !"
  	exit
  fi
WORKDIR=$1

LOCALDIR=$(pwd -P)

SCRIPT_DIR=$(getScriptPath)
PKG_FULL_NAME=$(basename ${SCRIPT_DIR})

PKGNAME=$(getPackageName $PKG_FULL_NAME)
VERSION=$(getVersion $PKG_FULL_NAME)

UPATH=$(getUtilitiesPath)



if [ -d "./umeea" ]
then
	mkdir -p $WORKDIR/opt/umeea   2>/dev/null
	cp -R ./umeea/* $WORKDIR/opt/umeea/   2>/dev/null
fi


if [ $PKGNAME == "base" ]; then
	AUTODEP=0
else 
	AUTODEP=1
fi
${UPATH}/buildDependencies.py $SCRIPT_DIR/dependencies $WORKDIR/DEBIAN/control $AUTODEP

prepareDEBIANscript $WORKDIR preinst
prepareDEBIANscript $WORKDIR postinst
  	
installEtcItem $WORKDIR
installDesktop $WORKDIR

finalizeDEBIANscript $SCRIPT_DIR $WORKDIR preinst
finalizeDEBIANscript $SCRIPT_DIR $WORKDIR postinst

cd $WORKDIR
cd ..

${UPATH}/finalizeDebianPackage.sh $PKGNAME $VERSION amd64

mv ./umeea-*.deb $SCRIPT_DIR
cd $LOCALDIR

}

unpack_deb()
{

  if [ "$#" -ne 2 ]; then
  	echo "Please give :"
  	echo " - debian package to unpack as first parameter"
  	echo " - working directory as second parameter "
  	exit
  fi

DEB=$1
UNPACKDIR=$2


ar x ${DEB} --output ${UNPACKDIR}
mkdir -p ${UNPACKDIR}/pkg/DEBIAN
tar -vxf ${UNPACKDIR}/control.tar.gz -C ${UNPACKDIR}/pkg/DEBIAN
tar -vxf ${UNPACKDIR}/data.tar.xz -C ${UNPACKDIR}/pkg

}


