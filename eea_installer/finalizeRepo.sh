#!/bin/bash

if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters :"
    echo " - first argument must be the debian repository local folder"
    echo " - second argument must be the GPGHome folder that contains both private and public keys"
    echo " - third argument must be the name of the GPGHome-stored private key (ex : umeea)"

    exit
fi


KEYNAME=$3     #umeea
GPGHOME=$2     #./GPGHome # was created with the createGPGKeys.sh script
REPO=$1

cd ${REPO}

#apt-ftparchive --arch amd64 packages amd64 > Packages
apt-ftparchive packages amd64 > Packages
gzip -k -f Packages
apt-ftparchive release . > Release
rm -rf Release.gpg
gpg --homedir ${GPGHOME} --default-key ${KEYNAME} -abs -o Release.gpg Release
rm -rf InRelease
gpg --homedir ${GPGHOME} --default-key ${KEYNAME} --clearsign -o InRelease Release
cp ${GPGHOME}/KEY.gpg $REPO

cd ..
