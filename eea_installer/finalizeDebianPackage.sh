#!/bin/bash

if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters :"
    echo " - first argument must be package name (ex : umeea),"
    echo " - second argument must be version number (ex: 1.01),"
    echo " - architecture (ex : amd64)"
    echo
    exit
fi
PKGNAME=umeea-$1 #umeea
VERSION=$2 #1.01
ARCHITECTURE=$3 #amd64


## Create the package
dpkg-deb --build ${PKGNAME}

mv ${PKGNAME}.deb ${PKGNAME}-${VERSION}_${ARCHITECTURE}.deb
