#!/bin/bash
# mikhael.myara@umontpellier.fr 2021
if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters :"
    echo " - should give repository folder name as parameter"
    echo
    exit
fi


REPODIR=$1

rm -rf ${REPODIR}
mkdir -p ${REPODIR}
mkdir -p ${REPODIR}/amd64/

echo Repository ${REPODIR} now created. You should :
echo - fill ${REPODIR}/amd64 folder with debian packages
echo - add the public GPG key with name KEY.gpg in ${REPODIR}
echo - call the finalizeRepo.sh with the GPG homedir as first argument and the secret key name as second argument

