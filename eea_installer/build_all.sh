#!/bin/bash
source "./utilities.sh"


# PPA version is calculated from international time by NIST
NIST_FULL_TIME=$(cat </dev/tcp/time.nist.gov/13)
NIST_DAY=$(echo ${NIST_FULL_TIME}|  awk '{print $2}')
NIST_TIME=$(echo ${NIST_FULL_TIME}|  awk '{printf("%s %s",$2,$3)}')
PPAVERSION=$(date -d ${NIST_DAY} +%Y%m%d).$(date -d "${NIST_TIME}" +%H%M.%S)
SHORTINFO="v. ${PPAVERSION} - "

# PATHs involved
SCRIPTPATH=$(getScriptPath)
PPAPATH=${SCRIPTPATH}/../PPA
export REPODIR=$(mktemp -d)

# PREPARE PPA
bigbanner "GENERATING PPA"

banner ${SHORTINFO}

${SCRIPTPATH}/prepareRepo.sh $REPODIR
> ${SCRIPTPATH}/all-extra-repositories.sh


# MAIN LOOP over all pkg-enabled.conf packages to create all umeea-debian packages
# also collect all the external repositories procedures and agregate them in all-extra-repositories.sh
while IFS= read -r pkg; do
  
if [[ ! $pkg = \#* ]] ; then

  all_pkg_found=$(find ${SCRIPTPATH}/pkg-available -maxdepth 1 -name "${pkg}-*" | sort -r)

  count=0
  if [ ! -z "$all_pkg_found" ]; then
  count=$(echo "${all_pkg_found}" | wc -l)
  fi

  
  if  ((count  > 1 )); then
    pkg_folder=$(echo "${all_pkg_found}"| head -n 1)
    bannerText="FOUND $count PACKAGES FOR ** $pkg ** - SELECTING HIGHEST VERSION"
  elif   ((count  == 0 )); then
     errorbanner "NO PACKAGES FOUND FOR ** $pkg ** ! EXITING WITH ERROR!"
     exit -1
  else 
    pkg_folder=$(echo "${all_pkg_found}"| head -n 1)
    bannerText="FOUND SINGLE PACKAGE FOR $pkg"
  fi
  
  bigbanner $pkg_folder
  banner "$bannerText"
    
  cd $pkg_folder
  ./build_me.sh
  mv ./*.deb ${REPODIR}/amd64
  if [ -f ./repositories ]; then
  	cat ./repositories >> ${SCRIPTPATH}/all-extra-repositories.sh
  fi
fi
done < pkg-enabled.conf




# BUILD umeea-extra-repositories package
TMPDIR=$(mktemp -d)
cd $TMPDIR
mkdir extra-repositories-${PPAVERSION}
cd extra-repositories-${PPAVERSION}

bigbanner $TMPDIR/extra-repositories-${PPAVERSION}

> ./build_me.sh 
echo "#!/bin/bash" >> ./build_me.sh 
echo "source ${SCRIPTPATH}/utilities.sh" >> ./build_me.sh 
echo 'banner EXTRA-REPOSITORIES' >> ./build_me.sh 
echo 'WORKDIR=$(preparePackage)' >> ./build_me.sh 
echo 'echo $WORKDIR' >> ./build_me.sh 
echo 'finalizePackage $WORKDIR' >> ./build_me.sh 

cp ${SCRIPTPATH}/all-extra-repositories.sh $TMPDIR/extra-repositories-${PPAVERSION}/postinst

chmod +x ./build_me.sh

./build_me.sh

mv ./*.deb ${REPODIR}/amd64

cd ${SCRIPTPATH}
# umeea-extra-repositories package BUILT


# BUILD umeea-full-install package
TMPDIR2=$(mktemp -d)
cd $TMPDIR2
mkdir full-install-${PPAVERSION}
cd full-install-${PPAVERSION}

bigbanner $TMPDIR/full-install-${PPAVERSION}

> ./build_me.sh 
echo "#!/bin/bash" >> ./build_me.sh 
echo "source ${SCRIPTPATH}/utilities.sh" >> ./build_me.sh 
echo 'banner FULL-INSTALL' >> ./build_me.sh 
echo 'WORKDIR=$(preparePackage)' >> ./build_me.sh 
echo 'echo $WORKDIR' >> ./build_me.sh 
echo 'finalizePackage $WORKDIR' >> ./build_me.sh 


> ./dependencies
for pkg in ${REPODIR}/amd64/*.deb
do
  PKGNAME=$(dpkg-deb -I ${pkg} |grep Package|awk -F":" '{print$2}'| xargs) 
  PKGVER=$(dpkg-deb -I ${pkg} |grep Version|awk -F":" '{print$2}'| xargs)   
  echo $PKGNAME "(>=$PKGVER)" >> dependencies
done

chmod +x ./build_me.sh

./build_me.sh

mv ./*.deb ${REPODIR}/amd64

cd ${SCRIPTPATH}
#  umeea-full-install package BUILT


# FINALIZE PPA
bigbanner "NOW BUILDING PPA"

${SCRIPTPATH}/finalizeRepo.sh ${REPODIR} ${SCRIPTPATH}/GPGHome umeea


rm -rf ${PPAPATH}
#mkdir -p  ${PPAPATH}
mv ${REPODIR} ${PPAPATH}

banner "full PPA is now available at ${PPAPATH}"

if test -f ${SCRIPTPATH}/post_build_all.sh ; then
 bigbanner "Running post generation script ..."
 ./post_build_all.sh
fi

