# Flash drive for Electrical Ingeneering students.
This projet of flash drive is supported by the teachers from "EEA" (electrical ingeneering) department from University of Montpellier, France.
It aims at building a Linux-based operating system containing basic scientific as well as hard science related open-source software to help students working by their own. It is thought to be used on a single USB flash drive on which the user may boot from the PC startup.

## Configuration under test (in french, sorry).
Préliminaire :
- VM VirtualBox 8 Go RAM 128 Go HDD, carte réseau en mode 'pont' (bridged adapter)
- Installation Linux Mint XFCE 20.1
- partitionnement : pas d'utilisation de LVM ici. Description :
```
Disque /dev/sda : 128 GiB, 137438953472 octets, 268435456 secteurs
Disk model: VBOX HARDDISK
Unités : secteur de 1 × 512 = 512 octets
Taille de secteur (logique / physique) : 512 octets / 512 octets
taille d'E/S (minimale / optimale) : 512 octets / 512 octets
Type d'étiquette de disque : dos
Identifiant de disque : 0xd05fc51f


Périphérique Amorçage     Début       Fin  Secteurs Taille Id Type
/dev/sda1    *             2048 195310641 195308594  93,1G 83 Linux
/dev/sda2             195313662 268433407  73119746  34,9G  5 Étendue
/dev/sda5             195313664 253904895  58591232    28G 83 Linux
/dev/sda6             253906944 268433407  14526464   6,9G 82 partition d'échange Linux / Solaris
```

- /dev/sda1 est mappé sur /
-  /dev/sda5 sur /home
-  /dev/sda6 est le swap  
- logiciels tiers multimeda (codecs) activés pendant l'Installation
- utilisateur : eea ; pass : 1 ; hostname : eea-Dist
- post installation dans un terminal :
```
sudo su
apt install openssh-server
apt install git
```
- *IMPORTANT* : to avoid conflicts, it is better to run the next steps through a ssh connection from another acount than the `eea` user acount, or from another computer. So you should close the `eea` user session once openssh and git are installed

## Generation of Debian Packages

First download the package by running the following commands :
```
sudo su
git clone https://gitlab.com/umeea/usb-drive.git
cd usb-drive
```
You require a GPG home directory, called GPGHome, placed inside the eea_installer folder.
If you have one, simply copy it. Else , you can generate it :
```
cd eea_installer
./createGPGKeys.sh
```
Then you can run the debian packages and repository builder script :
```
./build_all.sh
```

For now ... that's all. Then a folder called PPA is generated. It contents is an operational Debian repository. You can upload it to an http server. In this project we upload it in a ppa folder inside the eea department site : http://www.eea.univ-montp2.fr/ppa


## Install all
The script generate 3 additionnal packages :
- one called `base` of which all packages generated it depend. It allows for example to perform a clean uninstall.
- one called `extra-repositories`, that contains all the additional repositories code. It should be installed first from the repository, as many packages depend on extra repositories.
- a last one called 'full-install', that contains references to all THE packages of this repository.
So the installation procedure should work as follows (we use the eea dept site for the example):
 ```
wget -qO - http://www.eea.univ-montp2.fr/ppa/KEY.gpg | sudo apt-key add -
echo "deb http://www.eea.univ-montp2.fr/ppa/ /" | sudo tee /etc/apt/sources.list.d/umeea.list"
apt update
apt install umeea-extra-repositories
apt update
apt install umeea-full-install
```
That's all.


## Adding installation "packages"
The installer contains a list of installation packages in the folder `./eea_installer/pkg-available`. Each installation package is simply a custom script `build_me.sh` inside a folder which is the name of the package. This script is directly run by the global installer.
In the folder corresponding to a package, some files are specific : preinst, postinst, repositories and dependencies. These contain preinst and postinst code that will be embedded in the debian package, repositories contains additionnal code to add repositories (all the repositories code will be collected in a unique extra-repositories.deb package). dependencies contains one debian pacakge name per line which are the dependencies of this package. It is important that each folder name inside pkg-available contains the version number as xx.yy.zz after a dash at the end of its name.

To choose which packages are used, the user has to make, for each wanted package, a hard link in `./eea_installer/pkg-enabled`. This hard link has to points to the wanted package situated in `./eea_installer/pkg-available`. The name of the hard link is supposed to indicated in which position it will be run during the installation process, in order to manage some kind of dependency.
For example, the package `./eea_installer/pkg-available/anaconda` contains scripts for basic anaconda installation. It has been activated by :
```
ln -s ./eea_installer/pkg-available/anaconda ./eea_installer/pkg-available/120-anaconda
```

So configuring the installer works in a similar way as init.d

Adding a package thus consists in creating a new folder in `./eea_installer/pkg-available`, creating a script `install_me.sh` inside of it, that contains the installation instructions, and then performing a hard link of the folder to `./eea_installer/pkg-availabler` with an appropriate number to insert it the installation process
